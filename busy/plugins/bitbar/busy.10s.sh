#!/bin/bash
export PATH="$HOME/.pyenv/shims:/usr/local/bin:$PATH"
BUSY="`which busy`"
TASK="$(busy get-without-resource)"
TAGS="$(busy tags)"
QUEUES="$(busy queues)"
RESOURCE="$(busy resource)"
echo "$TASK | color=blue"
echo "---"
if [ -z "$RESOURCE" ]
then
  echo "Go | color=#C0C0C0"
else
  echo "Go | bash=/usr/bin/open param1=\"$RESOURCE\" terminal=false"
fi
echo "Finish | bash=$BUSY param1=finish param2=--yes refresh=true terminal=false"
echo "Pop | bash=$BUSY param1=pop refresh=true terminal=false"
for tag in $TAGS
do
  echo "-- $tag | bash=$BUSY param1=pop param2=$tag refresh=true terminal=false"
done
echo "Shuffle | bash=$BUSY param1=shuffle refresh=true terminal=false"
for tag in $TAGS
do
  echo "-- $tag | bash=$BUSY param1=shuffle param2=$tag refresh=true terminal=false"
done
echo "Drop | bash=$BUSY param1=drop refresh=true terminal=false"
echo "Delete | bash=$BUSY param1=delete param2=1 param3=--yes refresh=true terminal=false"
echo "Add | bash=$BUSY param1=add param2=--interactive refresh=true terminal=false"
echo "Manage Top | bash=$BUSY param1=manage param2=1 param3=--interactive refresh=true terminal=false"
echo "Queue"
for queue in $QUEUES
do
  echo "-- $queue | bash=$BUSY param1=get param2=--interactive param3=--queue param4=$queue terminal=false"
done
echo "Activate Today | bash=$BUSY param1=activate param2=--today refresh=true terminal=false"
